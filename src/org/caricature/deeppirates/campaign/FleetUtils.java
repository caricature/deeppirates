package org.caricature.deeppirates.campaign;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;

/**
 * @author caricature
 * Holds some methods for performing calculations about fleets, such as perceived relative strength, military vs. civilian, etc.
 */
public class FleetUtils {

	private static final float fighterSize = 1f;
	private static final float frigateSize = 1f;
	private static final float destroyerSize = 2.5f;
	private static final float cruiserSize = 5f;
	private static final float capitalSize = 10f;
	
	private static final float fighterAvgPts = 4f;
	private static final float frigateAvgPts = 5f;
	private static final float destroyerAvgPts = 10f;
	private static final float cruiserAvgPts = 30f;
	private static final float capitalAvgPts = 42f; //HT Guide
	
//	private static final float relsMagnConst = 2f;
	
	/**
	 * 
	 * @param ship - a fleet member
	 * @return an estimated strength
	 * It seems like some mods' ships might not be consistent in supporting the way I wanted to calculate this, so we just use weight class.
	 * Also I decided not to go with the game's default values.
	 * But, breaking this out allows for special casing certain ships, handling civilians, templars etc. anyway.
	 * TODO that
	 */
	public static float getShipStrength(FleetMemberAPI ship) {
		if (ship.isFighterWing()) {
			return fighterSize;
		} else if (ship.isFrigate()) {
			return frigateSize;
		} else if (ship.isDestroyer()) {
			return destroyerSize;
		} else if (ship.isCruiser()) {
			return cruiserSize;
		} else if (ship.isCapital()) {
			return capitalSize;
		} else { //Fallback
			System.out.println("Couldn't class ship " + ship.getId() + ", giving it " + ship.getDeploymentCostSupplies() * .2f + " points.");
			return ship.getDeploymentCostSupplies() * .2f;
		}
	}
	
	private static float getFleetSize(CampaignFleetAPI fleet) {
		float size = 0;
		for (FleetMemberAPI ship : fleet.getFleetData().getMembersListCopy()) {
			size += getShipStrength(ship);
		}
		return size;
	}
	
	private static float getFleetDeployPoints(CampaignFleetAPI fleet) {
		float points = 0;
		for (FleetMemberAPI ship: fleet.getFleetData().getMembersListCopy()) {
			points += ship.getDeploymentPointsCost();
		}
		return points;
	}
	
	/**
	 * 
	 * @param aifleet - the fleet to measure against the player.
	 * @return A measure of how strong the fleet thinks it is versus the player; higher is stronger.
	 * Unlike getRelativeStrength, this method factors in AI personality.
	 */
	public static float getPerceivedRelativeStrengthToPlayer(CampaignFleetAPI aifleet) {
		float rel = getRelativeStrength(aifleet, Global.getSector().getPlayerFleet());
		//Adjust for AI personality
		switch (aifleet.getCommander().getPersonalityAPI().getId()) {
		case "timid":
			System.out.println("Nerfing strength by 1 because the ai is timid."); //DEBUG
			rel -= 1;
		case "aggresive":
			System.out.println("Buffing strength by 1 because the ai is aggresive."); //DEBUG
			rel += 1;
		}
		return rel;
	}
	
	/**
	 * 
	 * @param a - a fleet to compare
	 * @param b - another fleet to compare
	 * @return higher the stronger a is compared to b; 0 if even, positive if a is stronger, negative if b is stronger.
	 * The $relativestrength value that can be accessed in rules.csv and so forth seems to just be -1,0, or 1, so for some purposes a more nuanced calculation is in order.
	 * Proportional to the ln of raw strength quotient.
	 */
	public static float getRelativeStrength(CampaignFleetAPI a, CampaignFleetAPI b) {
		float stra = getRawStrength(a);
		float strb = getRawStrength(b);
		System.out.println("Comparing " + a.getName() + " (strength " + stra + ") with " + b.getName() + " (strength " + strb +")"); //DEBUG
//		float abs = (float) (Math.log(stra) - Math.log(strb));
//		if (strb > stra) {
//			abs *= -1;
//		}
//		abs *= relsMagnConst; //Fudge Factor
//		System.out.println("Relative strength is " + abs); //DEBUG
//		return abs;
		float rels = stra/strb;
		rels -= 1;
		if (rels < 0) {
			rels = 1/rels;
		}
		System.out.println("Relative strength is " + rels); //DEBUG
		return rels;
	}
	
	/**
	 * 
	 * @param flt - a fleet
	 * @return A rude measure of how strong a fleet is.
	 */
	public static float getRawStrength(CampaignFleetAPI flt) {
		System.out.println("Getting raw strength for " + flt.getName()); //DEBUG
		//We start with a simple size count and then apply modifiers.
//		float strength = flt.getFleetSizeCount(); //Seems borked somehow.
		float strength = getFleetSize(flt);
		System.out.println("Size was " + strength); //DEBUG
		//Count the flagships twice, since they usually have the player or a good officer.
		//The player could exploit this, but the effect of doing so is usually very small.
		FleetMemberAPI flag = flt.getFlagship();
		if (flag.isFrigate()) {
			strength += 1;
		} else if (flag.isDestroyer()) {
			strength += 2;
		} else if (flag.isCruiser()) {
			strength += 3;
		} else if (flag.isCapital()) {
			strength += 5;
		}
		System.out.println("After flagship bonus, strength was " + strength); //DEBUG
		//We apply a multiplier based on the number of fleet points compared to arbitrary expected values.
		//In practice, this heavily penalizes (d) variants (in SS+ only?) and civilian hulls, and gives a mild preference for high-tech.
		//It doesn't matter too much if the average multiplier isn't near 1 because they are usually compared against each other.
		float expectedWeight = 0f;
		expectedWeight += fighterAvgPts * flt.getNumFighters();
		expectedWeight += frigateAvgPts * flt.getNumFrigates();
		expectedWeight += destroyerAvgPts * flt.getNumDestroyers();
		expectedWeight += cruiserAvgPts * flt.getNumCruisers();
		expectedWeight += capitalAvgPts * flt.getNumCapitals();
//		System.out.println("Got " + expectedWeight + " fleet points versus " + getFleetDeployPoints(flt) + " expected."); //DEBUG
//		//BEGIN DEBUG
//		for (FleetMemberAPI ship : flt.getFleetData().getMembersListCopy()) {
//			System.out.println("Ship " + ship.getHullId() + " Fleet Points " + ship.getFleetPointCost() + " Deploy Points " + ship.getDeploymentPointsCost()
//			+ " Supplies " + ship.getDeploymentCostSupplies());
//		} 
//		//END DEBUG
		expectedWeight = expectedWeight / getFleetDeployPoints(flt); //I like deploy points better than plain fleet points.
		//Fudge function to make this weighting matter less.
		expectedWeight = (float) Math.sqrt(expectedWeight);
		strength *= expectedWeight;
		System.out.println("After expected weight multiplier of " + expectedWeight + ", strength is " + strength); //DEBUG
		return strength;
	}
	
}
