package org.caricature.deeppirates;

import com.fs.starfarer.api.BaseModPlugin;

/**
 * 
 * @author caricature
 * This mod adds several features to player-pirate interactions, including:
 * Pirates take a "your money or your life" stance under certain circumstances
 * Acts of piracy will improve your standing.
 *
 */
public final class DeepPirates extends BaseModPlugin {
	
	/* And what an extension it is!*/

}
