package com.fs.starfarer.api.impl.campaign.rulecmd;

import java.util.List;
import java.util.Map;

import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.CargoAPI.CargoItemType;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.util.Misc.Token;

import com.fs.starfarer.api.Global;
//import com.fs.starfarer.api.impl.campaign.CoreReputationPlugin;
import com.fs.starfarer.api.impl.campaign.CoreReputationPlugin.*;
import com.fs.starfarer.api.campaign.RepLevel;

/**
 * @author caricature
 * This command is called up when the player decides how to deal with pirates extorting them, to take their cargo away (if applicable) and adjust their reputation.
 */
public class ApplyPirateExtortionEffects extends BaseCommandPlugin {

	/* (non-Javadoc)
	 * @see com.fs.starfarer.api.campaign.rules.CommandPlugin#execute(java.lang.String, com.fs.starfarer.api.campaign.InteractionDialogAPI, java.util.List, java.util.Map)
	 */
	// Simple details like subtracting credits are handled in rules.csv (for now at least).
	@Override
	public boolean execute(String ruleId, InteractionDialogAPI dialog,
			List<Token> params, Map<String, MemoryAPI> memoryMap) {
		CampaignFleetAPI bully = (CampaignFleetAPI) dialog.getInteractionTarget();
		CustomRepImpact impact = new CustomRepImpact();
		RepActionEnvelope envelope = new RepActionEnvelope(RepActions.CUSTOM, impact, dialog.getTextPanel());
		CargoAPI cargo = Global.getSector().getPlayerFleet().getCargo();
		MemoryAPI localMem = memoryMap.get(MemKeys.ENTITY); //TODO Ask why: for some strange reason, variables added to local (??in BeginFleetEncounter??) end up in entity.
		// See which way the interaction ended.
		switch (ruleId) {
				case "PbribePay":
				case "PbribePayN":
					cargo.getCredits().subtract(localMem.getFloat("$P_bribe"));
					break;
				case "PcargoPay":
				case "PcargoPayN":
					cargo.removeItems(CargoItemType.RESOURCES, localMem.get("$P_extortGoodID"), localMem.getFloat("$P_cargo"));
					break;
				default:
					System.out.println("Got unexpected choice " + ruleId);
		}
		//Reputation impact is the same no matter what (for now).
		//But the pirates are supposed to be hard, so.
		if (Math.random() < 0.5f) {
			impact.delta = RepRewards.SMALL;
		} else {
			impact.delta = RepRewards.TINY;
		}
		impact.limit = RepLevel.INHOSPITABLE;
		impact.requireAtBest = RepLevel.INHOSPITABLE; //Redundant?
		impact.requireAtWorst = RepLevel.HOSTILE;
		Global.getSector().adjustPlayerReputation(envelope, bully.getFaction().getId());
		return true;
	}

}
