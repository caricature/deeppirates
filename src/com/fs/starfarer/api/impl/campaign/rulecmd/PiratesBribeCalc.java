package com.fs.starfarer.api.impl.campaign.rulecmd;

import java.util.List;
import java.util.Map;

import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.impl.campaign.rulecmd.BaseCommandPlugin;
import com.fs.starfarer.api.util.Misc.Token;
import com.fs.starfarer.api.Global;

import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;

import org.caricature.deeppirates.campaign.FleetUtils;

/**
 * @author caricature
 * Holds the calculation for figuring out how much credits or cargo a player is going to have to bribe the pirates with.
 * The idea is that paying in cargo will be cheaper than paying in cash;
 * since the player probably wants the cargo more than the local market, this makes for interesting choices.
 * TODO: make this random (whether the pirates feel like being bribed (if not have execute return false), and how much they want) based on rep, relative force, aggression
 * ...Even if it's not apparently random, consistent bribes lets the player game the system with a precise pirate tax.
 */
public class PiratesBribeCalc extends BaseCommandPlugin {
	
	private static final float negotiationSurcharge = 1.5f;
	private static final float maxBribeStrength = 4f;
	private static final float minAttackStrength = 2f;
	
	//Lower number means higher precedence in bribing the pirates.
	private static int getVitality(String cid) {
		switch (cid) {
			case Commodities.GREEN_CREW:
			case Commodities.REGULAR_CREW:
			case Commodities.ELITE_CREW:
			case Commodities.VETERAN_CREW:
				return 2;
			case Commodities.FUEL:
			case Commodities.SUPPLIES:
				return 1;
			default:
				return 0;
				
		}
	}
	
	/* (non-Javadoc)
	 * @see com.fs.starfarer.api.campaign.rules.CommandPlugin#execute(java.lang.String, com.fs.starfarer.api.campaign.InteractionDialogAPI, java.util.List, java.util.Map)
	 */
	@Override
	public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Token> params, Map<String, MemoryAPI> memoryMap) {
		if (dialog == null) return false;
		//Figure out how strong the pirates are compared to us, to see whether they accept a bribe or just attack.
		CampaignFleetAPI pfleet;
		try {
			pfleet = (CampaignFleetAPI) dialog.getInteractionTarget();
		} catch (ClassCastException e) { //Player is not encountering a fleet.
			return false;
		}
		float rels = FleetUtils.getPerceivedRelativeStrengthToPlayer(pfleet);
		//TODO add an option where pirates may be bluffing, decide not to attack if you refuse bribe.
		//TODO make pirates deciding not to threaten you rely on this rather than the default calculation?
		//TODO make logic where the pirates decide to as for bribes rather than attack if the player is too fast to catch? (Hound A, etc.)
		float aggression = (float) Math.random(); //DEBUG
		float thresh = (rels - minAttackStrength) / (maxBribeStrength - minAttackStrength);
//		if ((rels - minAttackStrength)/(maxBribeStrength-minAttackStrength) > Math.random()) {
		if (thresh > aggression) { //DEBUG
			System.out.println("Rolled " + aggression + " versus " + thresh + ", pirates just attack."); //DEBUG
			return false;
		} else { //DEBUG
			System.out.println("Rolled " + aggression + " versus " + thresh + ", pirates ask for bribe."); //DEBUG
		}
		//First, work out how much value we want to extract from the player today. TODO make this reflect player expected wealth buffer, quadratic in fleet points or more.
		float bribeValue = (int) Global.getSector().getPlayerFleet().getFleetPoints() * 100; //Base bribe is small, but strength increases it.
		System.out.println("Raw bribe should be " + bribeValue + " credits worth of value."); //DEBUG
		rels += 1; //Modify and clamp to get a better multiplier.
		if (rels < 1) {
			rels = 1;
		}
		bribeValue *= rels;
		System.out.println("Relative strength is " + rels + " so asking for " + bribeValue + " credits of value ");
		// Work out the required bribe.
		float bribe = bribeValue; // DEBUG
		//Randomly decide whether the pirate captain is after cargo or credits.
		//If the player would rather pay in a different way, the price is double.
		if (Math.random() < 0.5f) {
			memoryMap.get(MemKeys.LOCAL).set("$P_cargoFirst", true, 0);
			bribe = bribe * negotiationSurcharge;
			System.out.println("Decided to want cargo, doubling bribe."); //DEBUG
		} else {
			bribeValue = bribeValue * negotiationSurcharge;
			System.out.println("Decided to want bribe, doubling cargo."); //DEBUG
		}
		// Work out the required taste of cargo.
		float heldCargoValue = 0f;
		String heldCargoId = null;
		float heldCargoQuant = 0f;
		CargoAPI playerCargo = Global.getSector().getPlayerFleet().getCargo();
		MarketAPI market = Global.getSector().getEconomy().getMarket(memoryMap.get(MemKeys.SOURCE_MARKET).getString("$id"));
		if (market == null) { //Not sure how often this happens; if it's normal, will need to add behavior.
			System.out.println("Orphan pirates don't take bribes!"); //DEBUG
			return false;
		}
		System.out.println("Getting prices from " + market.toString()); // DEBUG
		boolean enoughCargo = false;
		for (String cid : Global.getSector().getEconomy().getAllCommodityIds()) {
			float quant = playerCargo.getCommodityQuantity(cid);
			if (quant == 0f) {
				continue;
			}
//			System.out.println("Other is of type " + dialog.getInteractionTarget().getClass().toString()); // DEBUG
//			System.out.println("Interacting with " + dialog.getInteractionTarget().toString()); // DEBUG
			float value = market.getDemandPrice(cid, quant, false);
			System.out.println("The player has " + quant + " " + cid + ", worth " + value + " at " + market.getId()); // + DEBUG
//					" where the price is " + market.getDemandPrice(cid, 1, false) + "(base " + Global.getSector().getEconomy().getCommoditySpec(cid).getBasePrice() + ")");
			//Seems like getMarket is only used for actual markets. So we seem to have to try this:			
			//Take enough cargo to get the required value.
			quant = quant * (bribeValue/value);
			System.out.println("Therefore, we will ask for " + quant + " " + cid); // DEBUG
			boolean enoughOfThisGood = (quant <= playerCargo.getCommodityQuantity(cid)); //For extra sanity checking.
//			if (enoughOfThisGood) { //DEBUG
//				System.out.println("That's enough to pay the pirates."); //DEBUG
//			}
			boolean exchange = false;
			//Decide whether to use this good in place of whatever we have found before.
			//Pick the first commodity that will let the player pay the bribe, or failing that, the commodity worth the most.
			double switchChance = Math.random(); //DEBUG
			if (!enoughCargo && value > heldCargoValue) {
				exchange = true;
//			} else if ((Math.random() < value/(value+heldCargoValue)) || (!isVital(cid) && isVital(heldCargoId))) {
			//Switch to the good we are looking at if doing so will not cause us to fail when the existing good would have satisfied the pirate,
			//and if we pass a random check (weighted by the how much value is in each stack) or switching lets us avoid paying in fuel, crew, or supplies.
			} else if ((enoughOfThisGood || !enoughCargo) && ((switchChance < value/(value+heldCargoValue)) || (getVitality(cid) < getVitality(heldCargoId)))) { //DEBUG
				if (switchChance < value/(value+heldCargoValue)) { //DEBUG
					System.out.println("Rolled " + switchChance + " versus " + value/(value+heldCargoValue) + " so switching."); //DEBUG
				} else { //DEBUG
					System.out.println("Comparing nonvital " + cid + " to vital " + heldCargoId + " so switching."); //DEBUG
				} //DEBUG
				exchange = true;
			} else { //DEBUG
				System.out.println("Rolled " + switchChance + " versus " + value/(value+heldCargoValue) + "; not switching."); //DEBUG
			} //DEBUG
			if (exchange) {
				enoughCargo = enoughOfThisGood;	//Prevent rules.csv from wrongly thinking the player has enough, even if we screwed up and another good would have worked.
				heldCargoValue = value;
				heldCargoId = cid;
				heldCargoQuant = quant;
			}
		}
		int cargo = (int) Math.ceil(heldCargoQuant);
		memoryMap.get(MemKeys.LOCAL).set("$P_bribe", (int)bribe, 0);
		memoryMap.get(MemKeys.LOCAL).set("$P_cargo", (int)cargo, 0);
		memoryMap.get(MemKeys.LOCAL).set("$P_extortGoodID", heldCargoId, 0);
		memoryMap.get(MemKeys.LOCAL).set("$P_extortGood", Global.getSector().getEconomy().getCommoditySpec(heldCargoId).getName(), 0);
		if (bribe > Global.getSector().getPlayerFleet().getCargo().getCredits().get()) {
			System.out.println("The player can't afford the bribe."); //DEBUG
			memoryMap.get(MemKeys.LOCAL).set("$P_bribeTooHigh", true, 0);
		}
		if (!enoughCargo) {	//Written as if statement to be more agnostic about how the cargo bribe is calculated.
			System.out.println("The player hasn't enough cargo.");
			memoryMap.get(MemKeys.LOCAL).set("$P_cargoTooHigh", true, 0);
		}
		return true; //We converse no matter what, because it would be weird if the pirates magically know when you can't pay.
	}

}
